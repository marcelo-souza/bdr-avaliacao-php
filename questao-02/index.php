<?php
/**
 * Created by Marcelo.
 * Author: Marcelo Cardoso de Souza
 * Date: 06/08/2018
 * Time: 23:05
 */
$questao = 2;

$code = file_get_contents('refatorado.php');
$code = str_replace('<', '&lt;', $code);

?>
<html>
<head>
    <?php include '../view/partials/head.php'; ?>
</head>
<body>

<?php include '../view/partials/header.php'; ?>

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Refatore o código original, fazendo as alterações que julgar necessário</h3>
        </div>
        <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation"><a href="#original" aria-controls="original" role="tab" data-toggle="tab">Original</a></li>
                    <li role="presentation" class="active"><a href="#refatorado" aria-controls="refatorado" role="tab" data-toggle="tab">Refatorado</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane" id="original">
                          <pre>
&lt;?

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    header("Location: http://www.google.com");
    exit();
} elseif (isset($_COOKIE['Loggedin']) && $_COOKIE['Loggedin'] == true) {
    header("Location: http://www.google.com");
    exit();
}

                        </pre>
                    </div>
                    <div role="tabpanel" class="tab-pane active" id="refatorado">
                        <pre>
<?php print_r($code); ?>

                        </pre>
                    </div>
                </div>

        </div>
    </div>
</div>

<?php include '../view/partials/footer.php'; ?>

</body>
</html>
