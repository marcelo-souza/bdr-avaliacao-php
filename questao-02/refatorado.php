<?php
/**
 * Created by Marcelo.
 * Author: Marcelo Cardoso de Souza
 * Date: 06/08/2018
 * Time: 23:03
 */

const URL         = 'http://www.google.com';
const PATH_LOGGED = 'loggedin';

if ((isset($_SESSION[PATH_LOGGED]) && $_SESSION[PATH_LOGGED]) ||
    (isset($_COOKIE[PATH_LOGGED]) && $_COOKIE[PATH_LOGGED])
) {
    header(sprintf("Location:%s", URL));
    exit();
}