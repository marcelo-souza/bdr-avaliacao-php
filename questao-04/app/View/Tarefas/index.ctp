
<?php
// no seu arquivo de view
$this->Html->script('https://rawgit.com/isocra/TableDnD/master/js/jquery.tablednd.js', array('inline' => false));

$this->Html->script('list-tarefas', array('inline' => false));
?>

<div class="tarefas index">

    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?php echo __('Tarefas'); ?></h1>
            </div>
        </div><!-- end col md 12 -->
    </div><!-- end row -->

    <div class="row">

        <div class="col-md-3">
            <div class="actions">
                <div class="panel panel-default">
                    <div class="panel-heading">Ações</div>
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked">
                            <li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus text-success"></span>&nbsp;&nbsp;Nova tarefa'), array('action' => 'add'), array('escape' => false)); ?></li>
                        </ul>
                    </div><!-- end body -->
                </div><!-- end panel -->
            </div><!-- end actions -->
        </div><!-- end col md 3 -->

        <div class="col-md-9">

            <div class="pull-right">
                <ul class="nav nav-pills nav-stacked">
                    <li>
                    <?php echo $this->Html->link(__('<span class="glyphicon glyphicon-ok text-success"></span>&nbsp;&nbsp;Redefinir prioridade'), array('action' => 'priorizar'), array('escape'
                    => false, 'class' => 'btn-redefinir')); ?>
                    </li>
                </ul>
            </div>
            <table id="tab-tarefas" cellpadding="0" cellspacing="0" class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th><?php echo $this->Paginator->sort('id'); ?></th>
                        <th><?php echo $this->Paginator->sort('titulo'); ?></th>
                        <th><?php echo $this->Paginator->sort('descricao'); ?></th>
                        <th><?php echo $this->Paginator->sort('prioridade'); ?></th>
                        <th><?php echo $this->Paginator->sort('created'); ?></th>
                        <th><?php echo $this->Paginator->sort('modified'); ?></th>
                        <th class="actions"></th>
                    </tr>
                </thead>
                <tbody>
				<?php foreach ($tarefas as $tarefa): ?>
                    <tr>
                        <td class="text-center">
                            <input type="hidden" class="tarefa-id" value="<?php echo h($tarefa['Tarefa']['id']); ?>" />
                            <?php echo h($tarefa['Tarefa']['id']); ?>&nbsp;</td>
                        <td><?php echo h($tarefa['Tarefa']['titulo']); ?>&nbsp;</td>
                        <td><?php echo h($tarefa['Tarefa']['descricao']); ?>&nbsp;</td>
                        <td class="text-center"><?php echo h($tarefa['Tarefa']['prioridade']); ?>&nbsp;</td>
                        <td class="text-center"><?php echo h($tarefa['Tarefa']['created']); ?>&nbsp;</td>
                        <td class="text-center"><?php echo h($tarefa['Tarefa']['modified']); ?>&nbsp;</td>
                        <td class="actions">
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-search text-success"></span>', array('action' => 'view', $tarefa['Tarefa']['id']), array('escape' => false)); ?>
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-edit text-warning"></span>', array('action' => 'edit', $tarefa['Tarefa']['id']), array('escape' => false)); ?>
							<?php echo $this->Form->postLink('<span class="glyphicon glyphicon-remove text-danger"></span>', array('action' => 'delete', $tarefa['Tarefa']['id']), array('escape' =>
							false), __('Podemos excluir a tarefa %s?', $tarefa['Tarefa']['id'])); ?>
                        </td>
                    </tr>
				<?php endforeach; ?>
                </tbody>
            </table>

            <p>
                <small><?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?></small>
            </p>

			<?php
			$params = $this->Paginator->params();
			if ($params['pageCount'] > 1) {
			?>
            <ul class="pagination pagination-sm">
				<?php
					echo $this->Paginator->prev('&larr; Previous', array('class' => 'prev','tag' => 'li','escape' => false), '<a onclick="return false;">&larr; Previous</a>', array('class' => 'prev disabled','tag' => 'li','escape' => false));
					echo $this->Paginator->numbers(array('separator' => '','tag' => 'li','currentClass' => 'active','currentTag' => 'a'));
					echo $this->Paginator->next('Next &rarr;', array('class' => 'next','tag' => 'li','escape' => false), '<a onclick="return false;">Next &rarr;</a>', array('class' => 'next disabled','tag' => 'li','escape' => false));
				?>
            </ul>
			<?php } ?>

			<p class="text-primary text-left">* mova as linhas e clique em "Redefinir prioridade" para salvar a priorização desejada.</p>

        </div> <!-- end col md 9 -->
    </div><!-- end row -->


</div><!-- end containing of content -->