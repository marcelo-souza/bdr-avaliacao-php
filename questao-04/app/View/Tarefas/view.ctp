<div class="tarefas view">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1><?php echo __('Tarefa'); ?> <?php echo h($tarefa['Tarefa']['id']); ?></h1>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-3">
            <div class="actions">
                <div class="panel panel-default">
                    <div class="panel-heading">Actions</div>
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked">
                            <li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-edit text-warning"></span>&nbsp&nbsp;Editar tarefa'), array('action' => 'edit', $tarefa['Tarefa']['id']), array('escape' => false)); ?> </li>
                            <li><?php echo $this->Form->postLink(__('<span class="glyphicon glyphicon-remove text-danger"></span>&nbsp;&nbsp;Remover tarefa'), array('action' => 'delete', $tarefa['Tarefa']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $tarefa['Tarefa']['id'])); ?> </li>
                            <li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-list text-success"></span>&nbsp&nbsp;Listar tarefas'), array('action' => 'index'), array('escape' => false)); ?> </li>
                            <li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus text-success"></span>&nbsp&nbsp;Nova tarefa'), array('action' => 'add'), array('escape' => false)); ?> </li>
                        </ul>
                    </div><!-- end body -->
                </div><!-- end panel -->
            </div><!-- end actions -->
        </div><!-- end col md 3 -->

        <div class="col-md-9">			
            <table cellpadding="0" cellspacing="0" class="table table-striped">
                <tbody>
                    <tr>
                        <th><?php echo __('Título'); ?></th>
                        <td>
			                <?php echo h($tarefa['Tarefa']['titulo']); ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Descrição'); ?></th>
                        <td>
			                <?php echo h($tarefa['Tarefa']['descricao']); ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Prioridade'); ?></th>
                        <td>
			                <?php echo h($tarefa['Tarefa']['prioridade']); ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Data de criação'); ?></th>
                        <td>
			                <?php echo h($tarefa['Tarefa']['created']); ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('Data de última atualização'); ?></th>
                        <td>
			                <?php echo h($tarefa['Tarefa']['modified']); ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>

