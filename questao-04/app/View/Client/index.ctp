
<h1>Ações</h1>
<p>
    <ul>
        <li><?php echo $this->Html->link('Listar tarefas', array('controller' => 'client', 'action' => 'request_index')); ?></li>
        <li><?php echo $this->Html->link('Adicionar tarefa', array('controller' => 'client', 'action' => 'request_add')); ?></li>
        <li><?php echo $this->Html->link('Visualizar tarefa 1', array('controller' => 'client', 'action' => 'request_view', 1)); ?></li>
        <li><?php echo $this->Html->link('Atualizar tarefa 2', array('controller' => 'client', 'action' => 'request_edit', 2)); ?></li>
        <li><?php echo $this->Html->link('Excluir tarefa 3', array('controller' => 'client', 'action' => 'request_delete', 3)); ?></li>
    </ul>
</p>

