<?php
App::uses('AppController', 'Controller');

class TarefasController extends AppController
{

    public $helpers = array('Html', 'Form');
    public $paginate = array(
        'fields' => array('id', 'titulo', 'descricao', 'prioridade', "created", "modified"),
        'limit' => 25,
        'order' => array('prioridade' => 'asc')
    );

    public function index()
    {
        $this->set('tarefas', $this->paginate("Tarefa"));
    }

    public function add()
    {
        if ($this->request->is('post')) {
            $this->Tarefa->create();
            if ($this->Tarefa->save($this->request->data)) {
                $this->Session->setFlash(__('Tarefa salva com sucesso.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__('Não foi possível salvar sua tarefa.'));
        }
    }

    public function view($id = null)
    {
        if (!$id) {
            throw new NotFoundException(__('Tarefa inválida'));
        }

        $tarefa = $this->Tarefa->findById($id);
        if (!$tarefa) {
            throw new NotFoundException(__('Tarefa inválida'));
        }
        $this->set('tarefa', $tarefa);
    }

    public function edit($id = null)
    {
        if (!$id) {
            throw new NotFoundException(__('Tarefa inválida'));
        }

        $tarefa = $this->Tarefa->findById($id);
        if (!$tarefa) {
            throw new NotFoundException(__('Tarefa inválida'));
        }

        if ($this->request->is(array('tarefa', 'put'))) {
            $this->Tarefa->id = $id;
            if ($this->Tarefa->save($this->request->data)) {
                $this->Session->setFlash(__('Tarefa atualizada com sucesso'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__('Não foi possível atualizar sua tarefa.'));
        }

        if (!$this->request->data) {
            $this->request->data = $tarefa;
        }
    }

    public function delete($id)
    {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        if ($this->Tarefa->delete($id)) {
            $this->Session->setFlash(
                __('A tarefa com id: %s foi excluída.', h($id))
            );
            return $this->redirect(array('action' => 'index'));
        }
    }

    public function priorizar()
    {

        if ($this->request->is('get')) {

            $this->response->statusCode(200);
            $this->layout = false;
            $this->autoRender = false;
            
            $tarefasId = $this->request->query['tarefas'];

            $prioridade = 1;
            foreach ($tarefasId as $id) {
                $tarefa = $this->Tarefa->findById($id);

                if (!$tarefa) {
                    continue;
                }

                $tarefa['Tarefa']['prioridade'] = $prioridade;
                $this->Tarefa->id = $id;
                if (! $this->Tarefa->save($tarefa)) {

                }

                $prioridade ++;
            }

            return json_encode(array(
                'success' => 1,
                'message' => 'Prioridades definidas com sucesso.'
            ));

        }

        $this->response->statusCode(400);
        return json_encode(array(
            'success' => 0,
            'message' => 'Não foi possivel definir prioridades.'
        ));
    }
}
