<?php
App::uses('AppController', 'Controller');

class RestTarefasController extends AppController
{

    public $uses = array('Tarefa');
    public $helpers = array('Html', 'Form');
    public $components = array('RequestHandler');

    public function index()
    {
        $tarefas = $this->Tarefa->find('all');
        $this->set(array(
            'tarefas' => $tarefas,
            '_serialize' => array('tarefas')
        ));
    }

    public function add()
    {
        $this->Tarefa->create();
        if ($this->Tarefa->save($this->request->data)) {
            $message = 'Tarefa criada';
        } else {
            $message = 'Tarefa Erro';
        }
        $this->set(array(
            'message' => $message,
            '_serialize' => array('message')
        ));
    }

    public function view($id)
    {
        $tarefa = $this->Tarefa->findById($id);
        $this->set(array(
            'tarefa' => $tarefa,
            '_serialize' => array('tarefa')
        ));
    }

    public function edit($id)
    {
        $this->Tarefa->id = $id;
        if ($this->Tarefa->save($this->request->data)) {
            $message = 'Tarefa salva';
        } else {
            $message = 'Tarefa Erro';
        }
        $this->set(array(
            'message' => $message,
            '_serialize' => array('message')
        ));
    }

    public function delete($id)
    {
        if ($this->Tarefa->delete($id)) {
            $message = 'Tarefa Excluida';
        } else {
            $message = 'Tarefa Erro';
        }
        $this->set(array(
            'message' => $message,
            '_serialize' => array('message')
        ));
    }
}
