/**
 * Created by Marcelo on 07/08/2018.
 */
$(window).on('load', function () {


    $(document).ready(function () {
        // Initialise the first table (as before)
        $("#tab-tarefas").tableDnD();

        $('.btn-redefinir').click(function (e) {

            e.preventDefault();
            e.stopPropagation();

            var isRedefinir = confirm('Podemos redefinir a prioridade das tarefas?');
            if (isRedefinir) {

                var url = $(this).prop('href'),
                    data = {};

                var tarefas = [];

                $('.tarefa-id').each(function () {
                    tarefas.push($(this).val());
                });
                data['tarefas'] = tarefas;

                $.ajax({
                    url: url,
                    type: 'GET',
                    data: data,
                    beforeSend: function () {

                    },
                    success: function (data) {
                        alert('Tarefas priorizadas com sucesso.');
                        window.location = location.search;
                    },
                    error: function (data) {
                        alert('Não foi possível priorizar tarefas.');
                    },
                    complete: function (data) {

                    }
                });
            }
        });


    });

});