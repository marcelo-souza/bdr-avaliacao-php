<?php
/**
 * Created by Marcelo.
 * Author: Marcelo Cardoso de Souza
 * Date: 06/08/2018
 * Time: 22:32
 */

require_once 'Dao/UserRepository.php';

use Dao\UserRepository;

class MyUserClass
{

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * MyUserClass constructor.
     */
    public function __construct()
    {
        /** @var UserRepository $repository */
        $this->repository = new UserRepository();
    }

    /**
     * Lista usuários cadastrados
     *
     * @return array
     */
    public function getUserList()
    {
        try {

            return $this->repository->findAll();

        } catch (PDOException $p) {
            echo 'Erro de conexão: ' . $p->getMessage();
        } catch (Exception $e) {
            echo 'Ocorreu um erro inesperado: ' . utf8_decode($e->getMessage());
        }
    }

}