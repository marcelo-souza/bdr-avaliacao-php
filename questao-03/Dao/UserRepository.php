<?php
/**
 * Created by Marcelo.
 * Author: Marcelo Cardoso de Souza
 * Date: 06/08/2018
 * Time: 23:50
 */

namespace Dao;


require_once 'DatabaseConnection.php';

/**
 * Class User
 * @package Dao
 */
class UserRepository
{

    /**
     * @var databaseConnection
     */
    private $db;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->db = DatabaseConnection::getInstance();
    }

    /**
     * Obtem lista dos usuários.
     * @return array
     * @throws \PDOException
     * @throws \Exception
     */
    public function findAll()
    {
        try {
            $query = $this->db->prepare('SELECT name FROM user');
            $query->execute();

            $results = $query->fetchAll();
            sort($results);

            return $results;

        } catch (\Exception $e) {
            throw $e;
        }
    }

}