<?php
/**
 * Created by Marcelo.
 * Author: Marcelo Cardoso de Souza
 * Date: 06/08/2018
 * Time: 23:43
 */

namespace Dao;

include 'config/db.php';

use PDO;

/**
 * Class DatabaseConnection
 * @package Dao
 */
class DatabaseConnection
{

    /** Contantes definidas no arquivo de configuração localhost.php */
    CONST DB = DB_NAME;

    private $host = DB_HOST;
    private $user = DB_USER;
    private $password = DB_PASSWORD;

    /**
     * Instância singleton
     * @var DatabaseConnection
     */
    private static $instance;

    /**
     * Conexão com o banco de dados
     * @var PDO
     */
    private static $connection;

    /**
     * Construtor privado da classe singleton
     */
    private function __construct($host = DB_HOST, $user = DB_USER, $password = DB_PASSWORD)
    {
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;

        self::$connection = new PDO("mysql:dbname=" . self::DB . ";host=" . $host, $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        self::$connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
    }

    /**
     * Retorna uma instancia de DatabaseConnection
     * @return DatabaseConnection
     */
    public static function getInstance()
    {

        if (empty(self::$instance)) {
            self::$instance = new DatabaseConnection();
        }

        return self::$instance;
    }

    /**
     * Retorna a conexão com o banco de dados
     * @return PDO
     */
    public static function getConnection()
    {
        self::getInstance();

        return self::$connection;
    }

    /**
     * @param $sql
     * @return \PDOStatement
     */
    public static function prepare($sql)
    {
        return self::getConnection()->prepare($sql);
    }

}