<?php
/**
 * Created by Marcelo.
 * Author: Marcelo Cardoso de Souza
 * Date: 06/08/2018
 * Time: 22:32
 */

require_once 'refatorado.php';

$questao = 3;

$code = file_get_contents('refatorado.php');
$code = str_replace('<', '&lt;', $code);

$userRepository = file_get_contents('Dao/UserRepository.php');
$userRepository = str_replace('<', '&lt;', $userRepository);

$databaseConnection = file_get_contents('Dao/DatabaseConnection.php');
$databaseConnection = str_replace('<', '&lt;', $databaseConnection);

$config = file_get_contents('config/db.php');
$config = str_replace('<', '&lt;', $config);

$msgError = '';
$usersList = [];

try {

    /** @var MyUserClass $user */
    $user = new MyUserClass();

    $usersList = $user->getUserList();

} catch (PDOException $p) {
    $msgError = 'Erro de conexão: ' . $p->getMessage();
} catch (Exception $e) {
    $msgError = 'Ocorreu um erro inesperado: ' . utf8_decode($e->getMessage());
}

?>
<html>
<head>
    <?php include '../view/partials/head.php'; ?>
</head>
<body>

<?php include '../view/partials/header.php'; ?>

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Refatore o código original, fazendo as alterações que julgar necessário</h3>
        </div>
        <div class="panel-body">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="#original" aria-controls="original" role="tab" data-toggle="tab">Original</a></li>
                <li role="presentation" class="active"><a href="#refatorado" aria-controls="refatorado" role="tab" data-toggle="tab">Refatorado</a></li>
                <li role="presentation"><a href="#UserRepository" aria-controls="UserRepository" role="tab" data-toggle="tab">Dao/UserRepository.php</a></li>
                <li role="presentation"><a href="#DatabaseConnection" aria-controls="DatabaseConnection" role="tab" data-toggle="tab">Dao/DatabaseConnection.php</a></li>
                <li role="presentation"><a href="#config" aria-controls="config" role="tab" data-toggle="tab">config/db.php</a></li>
                <li role="presentation"><a href="#exemplo" aria-controls="exemplo" role="tab" data-toggle="tab">Exemplo de Uso</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">

                <div role="tabpanel" class="tab-pane" id="original">
                          <pre>
&lt;?

class MyUserClass
{
    public function getUserList()
    {
        $dbconn = new DatabaseConnection('localhost','user','password');
        $results = $dbconn->query('select name from user');

        sort($results);

        return $results;
    }
}

                        </pre>
                </div>

                <div role="tabpanel" class="tab-pane active" id="refatorado">
                    <pre>
<?php print_r($code); ?>

                    </pre>
                </div>

                <div role="tabpanel" class="tab-pane" id="UserRepository">
                    <pre>
<?php print_r($userRepository); ?>
                    </pre>
                </div>

                <div role="tabpanel" class="tab-pane" id="DatabaseConnection">
                    <pre>
<?php print_r($databaseConnection); ?>
                    </pre>
                </div>

                <div role="tabpanel" class="tab-pane" id="config">
                    <pre>
<?php print_r($config); ?>
                    </pre>
                </div>

                <div role="tabpanel" class="tab-pane" id="exemplo">
                    <div>
                        <?php if(! empty($msgError)) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?php echo $msgError; ?>
                            </div>
                        <?php endif; ?>
                        <div>
                            <h3>Lista de usuários</h3>
                        </div>
                        <div>
                            <table cellpadding="0" cellspacing="0" class="table table-striped table-hover">
                                <tr>
                                    <th>Nome</th>
                                </tr>
                                <?php foreach ($usersList as $repository): ?>
                                    <tr>
                                        <td><?php echo $repository->name; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<?php include '../view/partials/footer.php'; ?>

</body>
</html>