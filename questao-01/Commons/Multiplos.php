<?php
/**
 * Created by Marcelo.
 * Author: Marcelo Cardoso de Souza
 * Date: 06/08/2018
 * Time: 23:05
 */

namespace Commons;

/**
 * Class Multiple
 * @package Commons
 */
class Multiplos
{

    /**
     * Retorna a lista de números múltiplos dentro do intervalo informado
     * @param int $inicio inicio do intervalo
     * @param int $fim fim do intervalo
     *
     * @return string
     */
    public static function listaMultiplos($inicio, $fim)
    {
        $result = "";

        for ($i = $inicio; $i <= $fim; $i++) {

            $out = '';

            if (!($i % 3)) {
                $out .= 'Fizz';
            }

            if (!($i % 5)) {
                $out .= 'Buzz';
            }

            if (empty($out)) {
                $out = $i;
            }

            $result .= "<div class='row'>" . $out . "</div>";

        }

        return $result;
    }

}
