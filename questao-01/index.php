<?php
/**
 * Created by Marcelo.
 * Author: Marcelo Cardoso de Souza
 * Date: 06/08/2018
 * Time: 22:36
 */

require_once 'Commons/Multiplos.php';

$questao = 1;

use Commons\Multiplos;

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    header("Location: http://www.google.com");
    exit();
} elseif (isset($_COOKIE['Loggedin']) && $_COOKIE['Loggedin'] == true) {
    header("Location: http://www.google.com");
    exit();
}

?>
<html>
<head>
    <?php include '../view/partials/head.php'; ?>
</head>
<body>

<?php include '../view/partials/header.php'; ?>

<div class="container">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Escreva um programa que imprima números de 1 a 100. Mas, para múltiplos de 3 imprima
                “Fizz” em vez do número e para múltiplos de 5 imprima “Buzz”. Para números múltiplos
                de ambos (3 e 5), imprima “FizzBuzz”.</h3>
        </div>
        <div class="panel-body">
                <div class="container">
                    <?php echo Multiplos::listaMultiplos(1, 100); ?>
                </div>
        </div>
    </div>
</div>
</div>

<?php include '../view/partials/footer.php'; ?>

</body>
</html>
