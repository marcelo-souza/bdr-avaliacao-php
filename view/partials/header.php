<?php
/**
 * Created by Marcelo.
 * Author: Marcelo Cardoso de Souza
 * Date: 06/08/2018
 * Time: 22:40
 */
$active = "active";

$active01 = "";
$active02 = "";
$active03 = "";
$active04 = "";

switch ($questao) {
    case 1:
        $active01 = $active;
        break;

    case 2:
        $active02 = $active;
        break;

    case 3:
        $active03 = $active;
        break;

    case 4:
        $active04 = $active;
        break;
}


?>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Avaliação BDR</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="<?php echo $active01; ?>"><a href="/questao-01">Questão 01</a></li>
                <li class="<?php echo $active02; ?>"><a href="/questao-02">Questão 02</a></li>
                <li class="<?php echo $active03; ?>"><a href="/questao-03">Questão 03</a></li>
                <li class="<?php echo $active04; ?>"><a href="/questao-04">Questão 04</a></li>
            </ul>
        </div>
    </div>
</nav>
