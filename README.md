# Teste de Conhecimentos – Analista Desenvolvedor PHP

- Cada questão esta organizada em diretorios separados.
- Para visualizar todas as resoluções basta disponibilizar o projeto 
num servidor web que rode PHP 5.3 e Mysql e acessar o arquivo index.php na raiz do projeto.
- Para se acessar cada questão basta navegar pelos menus na barra 
superior.
- As questões 03 e 04 utilizam base de dados e para isso deve ser 
executado o script sql que esta no diretorio resource/avaliacao-bdr.sql


## Questão 1 - Diretório "questao-01"

Escreva um programa que imprima números de 1 a 100. Mas, para múltiplos de 3 imprima “Fizz” em vez do número e para múltiplos de 5 imprima “Buzz”. Para números múltiplos de ambos (3 e 5), imprima “FizzBuzz”.

### Solução: 

Criamos classe especifica com método que rebecer um valores de inicio e fim para retornar a string se estes são multiplos de 3(Fizz), 5(Buzz) ou ambos(FizzBuzz). 


## Questão 2 - Diretório "questao-02"

Refatore o código abaixo, fazendo as alterações que julgar necessário.
```php
<?

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
	header("Location: http://www.google.com");
	exit();
} elseif (isset($_COOKIE['Loggedin']) && $_COOKIE['Loggedin'] == true) {
	header("Location: http://www.google.com");
	exit();
}
```

### Solução: 

Criado novo script com o codigo refatorado

## Questão 3 - Diretório "questao-03"

Refatore o código abaixo, fazendo as alterações que julgar necessário.
```php
<?php

class MyUserClass
{
	public function getUserList()
	{
		$dbconn = new DatabaseConnection('localhost','user','password');
		$results = $dbconn->query('select name from user');

		sort($results);

		return $results;
	}
}
```

### Solução: 

- Criada nova classe responsável por fazer conexão com banco de dados utilizando padrão Singleton. 
- Criada classe de repository responsável efetuar as busca das 
informações destes, porém ela tem o intuito de ser responsável por todo CRUD de usuários.
- Criado um arquivo para armazenar as configuração para acesso ao banco de dados;

### Requisitos
* PHP 5.3 ou superior.
* MySQL

### Instalação

- Reconfigure os dados de acesso a banco no arquivo "config/db.php" ajustando o "host", "username" e "password"
- Acesse atraves do menu superior o link para a Questão 3 


## Questão 4 - Diretório "questao-04"

Desenvolva uma API Rest para um sistema gerenciador de tarefas (inclusão/alteração/exclusão). As tarefas consistem em título e descrição, ordenadas por prioridade.

Desenvolver utilizando:
    * Linguagem PHP (ou framework CakePHP);
    * Banco de dados MySQL;
Diferenciais:
    * Criação de interface para visualização da lista de tarefas; (Implementado)
    * Interface com drag and drop;
    * Interface responsiva (desktop e mobile); (Implementado)

### Solução: 

- Desenvolvida Rest API que suporta inclusão/alteração/exclusão/listagem
- Desenvolvida interface de testes para as requisições JSON da Rest API
- Desenvolvida interface para manutenção das tarefas

Desenvolvido utilizando PHP 5.3 e framework CakePHP 2.6.

### Requisitos
* Servidor HTTP com mod_rewrite habilitado.
* PHP 5.3 ou superior.
* MySQL

### Instalação

- Colocar a pasta "questao-04" num servidor apache
- Reconfigurar os dados de acesso a banco no arquivo "questao-04/app/Config/database.php" ajustando "host", "username" e "password"
  
